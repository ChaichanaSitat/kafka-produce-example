package com.kafka.produce.demo.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

@Configuration
public class KafkaProducerConfig {

	@Value("${spring.kafka.producer.bootstrap-servers}")
	private String brokers;
	
	@Value("${spring.kafka.producer.key-serializer}")
	private String kafkaKeySerializer;

	@Value("${spring.kafka.producer.value-serializer}")
	private String kafkaValueSerializer;
	
	
	
    @Bean
    public ProducerFactory<String, String> producerFactory() {
        Map<String, Object> topic1Props = new HashMap<>();
        topic1Props = new HashMap<>();
        topic1Props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, brokers);
        topic1Props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, kafkaKeySerializer);
        topic1Props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, kafkaValueSerializer);
        return new DefaultKafkaProducerFactory<>(topic1Props);
    }

    @Bean
    public KafkaTemplate<String, String> kafkaTemplate() {
        return new KafkaTemplate<>(producerFactory());
    }
     
    
}
