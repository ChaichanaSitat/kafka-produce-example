package com.kafka.produce.demo.service;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class MessageSender {
	
	private final KafkaTemplate<String, String> kafkaTemplate;
	
	   public MessageSender(KafkaTemplate<String, String> kafkaTemplate) {
	        this.kafkaTemplate = kafkaTemplate;
	    }

	    public void sendToKafka(String topic, String message) {
	        kafkaTemplate.send(topic, message).completable();
	    }

}
