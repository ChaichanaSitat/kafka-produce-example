package com.kafka.produce.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kafka.produce.demo.bean.Chaichana;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class KafkaService {

	@Value("${spring.kafka.producer.topic}")
	private String chaichanaTopic;

	@Value("${spring.kafka.producer.topic2}")
	private String ballzaTopic;

	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private MessageSender messageSender;
	
	public void sendChaichana(Chaichana ball) throws JsonProcessingException {
		log.info("Send Chaichana : {}", ball);
		this.messageSender.sendToKafka(chaichanaTopic, this.objectMapper.writeValueAsString(ball));
	}
	
	public void sendBallza(Chaichana ball) throws JsonProcessingException {
		log.info("Send Ballza : {}", ball);
		this.messageSender.sendToKafka(ballzaTopic, this.objectMapper.writeValueAsString(ball));
	}

}
