package com.kafka.produce.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kafka.produce.demo.bean.Chaichana;
import com.kafka.produce.demo.service.KafkaService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class ProduceController {

	@Autowired
	KafkaService kafkaservice;

	@GetMapping("/produce")
	public String getName() throws JsonProcessingException {

		kafkaservice.sendChaichana(new Chaichana());
		kafkaservice.sendBallza(new Chaichana());

		return "Success";
	}

}
