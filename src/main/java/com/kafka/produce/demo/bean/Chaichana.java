package com.kafka.produce.demo.bean;


import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Chaichana {
	
	private String name = "chaichana";
	private String lastName = "sitat";
	private String nickName = "ball";
	private String status = "single";
	private String phoneNumber = "096-8130xx8";
	@JsonFormat(pattern = "yyyy-MM-dd : HH:mm:ss")
	private LocalDateTime date = LocalDateTime.now(); 

}
